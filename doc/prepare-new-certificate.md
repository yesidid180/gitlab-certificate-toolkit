# Preparing a new certificate for deployment

In order to deploy a certificate we need to do the following things:
- assemble the leaf certificate and the intermediate certificates into the
  server certificate chain;
- verify the chain;
- copy the server certificate chain to the production server;
- verify that the leaf certificate matches the server key.

## 1. Assemble the server certificate chain

This step probably happens on your own machine, not the server.
Save the certificate provided by the CA as a .pem file in your
gitlab-certificate-toolkit directory. We assume the file is called
`www.example.com.pem`.

Check the issuer of your certificate:

```
openssl x509 -noout -issuer -in www.example.com.pem
```

Look for the matching certificate chain in the `intermediate/` directory. Below
we assume the issuer was 'COMODO RSA Domain Validation Secure Server CA' and we
use the `intermediate/comodo-rsa-domain-validation-secure-server-ca.bundle`
bundle.

Create the server certificate chain:

```
bin/glue-certs www.example.com.pem intermediate/comodo-rsa-domain-validation-secure-server-ca.bundle > www.example.com.crt
```

## 2. Verify the certificate chain

We want to double-check that the certificates in `www.example.com.crt` actually
form a chain.

```
bin/verify-chain-signatures www.example.com.crt
```

Example output:

```
$ bin/verify-chain-signatures ci.gitlap.com.crt 
Subject: "ci.gitlap.com"
OK: "COMODO RSA Domain Validation Secure Server CA" signed "ci.gitlap.com"
OK: "COMODO RSA Certification Authority" signed "COMODO RSA Domain Validation Secure Server CA"
OK: /usr/local/etc/openssl trusts "COMODO RSA Certification Authority"
```

## 3. Copy the server certificate chain to the production server

Copy the chain to your home directory on www.example.com.

```
scp www.example.com.crt www.example.com:~
```

If you need to specify the SSH user you can do so as follows.

```
scp www.example.com.crt root@www.example.com:~
```

If you are using Chef, you can convert the certificate chain to a JSON string
as follows:

```
ruby -rjson -e 'puts JSON.dump(ARGF.read)' www.example.com.crt
```

## 4. Verify that the leaf certificate matches the server key

This step is done on the production server because that is where the key is.

We assume you generated the key on the server in `/root/`. First copy the
certificate chain to `/root` if necessary.

```
# If the .crt is in /home/myuser/www.example.com.crt
sudo cp -n ~/www.example.com.crt /root/
```

Now start a root shell and define the `match_key_and_cert` function.

```
sudo -i # start an interactive shell as root

match_key_and_cert()
{
  printf "key modulus MD5: "
  openssl rsa -noout -modulus -in "$1" | openssl md5
  printf "crt modulus MD5: "
  openssl x509 -noout -modulus -in "$2" | openssl md5
}
```

Compare the key and the certificate. The MD5 strings should be the same.

```
match_key_and_cert www.example.com.key www.example.com.crt
```

Done! You now have a key and certificate chain on your server ready for
deployment.