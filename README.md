# GitLab Certificate Toolkit

This repository contains documentation and tooling for handling SSL (HTTPS)
certificates at GitLab B.V. We are sharing this information as an example of
how we tried to streamline and standardize working with SSL certificates by
collecting documentation, tooling and resources in a single Git repository.
This repository is not meant to be directly applicable in other teams.

Handling SSL certificates can be intimidating (one mistake and your website
becomes unreachable due to browser certificate warnings) and cumbersome
(copy-pasting lots of OpenSSL commands you do not really understand).

In this repository, we
- standardize GitLab B.V. certificate requests (do you remember what 'locality'
  the certificate should be for?);
- collect valid intermediate certificate chains for Certificate Authorities we
  work with (avoid fruitless searches on CA support forums);
- offer tools to verify the integrity of a certificate chain (leaf certificate
  plus intermediates), and whether a key and certificate belong together.

We do this using Ruby scripts (for handling the certificate chain on a local
machine) and shell functions (to assist with complex one-time tasks on remote
servers).

## Generate a new private key and CSR

See [doc/request-new-certificate.md](doc/request-new-certificate.md).

## Prepare a new certificate for deployment

See [doc/prepare-new-certificate.md](doc/prepare-new-certificate.md).

## Install a new certificate

See [doc/install-new-certificate.md](doc/install-new-certificate.md).

## Intermediate certificates

Intermediate certificate chains for CA's commonly used by GitLab B.V. can be
found in the 'intermediate/' directory.

## Verify a server certificate chain

The `bin/verify-chain-signatures` script uses Ruby. You probably want to run it
locally instead of on the web server.

```
# Verify a .crt
bin/verify-chain-signatures foo.example.com.crt

# Verify the components of a .crt
cat foo.example.com.pem intermediate/essential-ssl-ca.bundle | bin/verify-chain-signatures
```

Example output:

```
$ bin/verify-chain-signatures chef.gitlab.com.crt
Subject: "chef.gitlab.com"
OK: "COMODO RSA Domain Validation Secure Server CA" signed "chef.gitlab.com"
OK: "COMODO RSA Certification Authority" signed "COMODO RSA Domain Validation Secure Server CA"
OK: /usr/local/etc/openssl trusts "COMODO RSA Certification Authority"
```

## Generate a self-signed certificate

The following shell function lets you quickly generate a self-signed
certificate. If you want to use this function first copy-paste the code below
into a shell session.

```
# shell function to generate a self-signed certificate
# Usage: self_signed_cert www.example.com
# Will create www.example.com.crt and www.example.com.key in the current directory
self_signed_cert() {
  (
    umask 077
    openssl req -new -subj "/CN=$1/" -x509 -days 365 -newkey rsa:2048 -nodes -keyout "$1.key" -out "$1.crt"
  )
}
```

Tip: if you want to add the resulting certificate to the local trust store on
an Mac OS X machine, copy the `www.example.com.crt` file to your Mac and run:

```
security add-trusted-cert www.example.com.crt
```

No more certificate warnings in your browser.

## Check when the certificate of a remote host expires

```
bin/get-remote-cert-expiry gitlab.com
```
